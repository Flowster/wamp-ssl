## Introduction

Wamp SSL is a Windows batch file that allows you to create SSL certificates for your local projects.

## Description

I needed to develop locally on my computer websites using HTTPS.

Wamp doesn't offer natively a way to generate a certificate recognised as valid by the browser.

This script will create for you :

 * An authority certificate that will automatically be installed on your computer
 * Virtualhosts on port 80 (http)
 * Virtualhosts on port 443 (https)
 * Certificates matching those virtualhosts

## Instructions

 1. Download the bat file in this repository
 2. Put this file inside a directory (ex: "vhosts") at the root of your Wamp installation
 3. Right click on this file and execute it with the administrator privilege
 4. Fill in the required settings :
   * Your local domain name: "dev.test.local"
   * Your local public app document root: "C:\wamp64\www\test\www"
   * Your local public app logs directory: "C:\wamp64\www\test\logs"
   * Apache openssl.exe path [should be autofilled]: (press enter if you see a path, or indicate the path otherwise)
   * Certificate authority domain name [my.root.ca]: (press enter or change the default value if you need to)
   * Certificate authority passphrase [mypassphrase]: (press enter or change the default value if you need to)
 5. You need to uncomment "extension=php_openssl.dll" in your "php.ini" file.
 6. You need to add "Include C:\wamp64\vhosts\httpd_ssl.cnf" in your "httpd.conf" file.
 7. You need to restart your Apache server
 8. You need to restart your browser
   