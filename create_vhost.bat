@echo off

net session >nul 2>&1
if not %errorLevel% == 0 (
    echo [41mThis program needs to be run as Administrator. [0m
    PAUSE
    EXIT
)

set "destination=%~dp0"
set foundopenssl=openssl.exe

if exist %destination%/../bin (
	cd %destination%/../bin
	dir /b openssl.exe /s > %temp%\found_openssl
	set /p foundopenssl=<%temp%\found_openssl
	del %temp%\found_openssl
	cd %destination%
)

echo [33mCreation of a self-signed certificat for local usage [0m
echo ===================
set /p domain="Your local domain name: "
set /p documentroot="Your local public app document root: "
set /p logs="Your local public app logs directory: "
if %foundopenssl%=="openssl.exe" (
	set /p opensslexe="Apache openssl.exe path: "
) else (
    set /p opensslexe="Apache openssl.exe path [%foundopenssl%]: " || set opensslexe=%foundopenssl%
)
set /p cadomain="Certificate authority domain name [my.root.ca]: " || set cadomain=my.root.ca
set /p passphrase="Certificate authority passphrase [mypassphrase]: " || set passphrase=mypassphrase

set ALTNAME=DNS:%domain%
set "RANDFILE=%destination%.rnd"
set "openssl_cnf=%destination%openssl.cnf"
set "openssl_ext_cnf=%destination%openssl-ext.cnf"
set "httpd_ssl_cnf=%destination%httpd_ssl.cnf"
set "ca_crt=%destination%%cadomain%.crt"
set "ca_key=%destination%%cadomain%.key"
set "domain_csr=%destination%%domain%.csr"
set "domain_crt=%destination%%domain%.crt"
set "domain_key=%destination%%domain%.key"
set "domain_key_nopass=%destination%%domain%.nopass.key"

if not exist %destination% (
	echo [41mThe certificates folder doesn't exist! Create it before running this command again. [0m
	PAUSE
	EXIT
)

if not exist %documentroot% (
	echo [41mThe document root folder doesn't exist! Create it before running this command again. [0m
	PAUSE
	EXIT
)

if not exist %logs% (
	echo [41mThe logs folder doesn't exist! Create it before running this command again. [0m
	PAUSE
	EXIT
)

if not exist %opensslexe% (
	echo [41mopenssl.exe can't be found! [0m
	PAUSE
	EXIT
)

if not exist %openssl_cnf% (
	echo [33mCreation of the conf file %openssl_cnf% [0m
	(
		echo HOME = .
		echo x509_extensions = v3_ca
		echo req_extensions = v3_req
		echo [ req ]
		echo distinguished_name	= req_distinguished_name
		echo [ req_distinguished_name ]
		echo countryName = Country Name ^(2 letter code^)
		echo [ v3_ca ]
		echo subjectKeyIdentifier=hash
		echo authorityKeyIdentifier=keyid:always,issuer
		echo basicConstraints = critical,CA:true
		echo [ v3_req ]
		echo basicConstraints = CA:FALSE
		echo keyUsage = nonRepudiation, digitalSignature, keyEncipherment
	) > %openssl_cnf%
)

if not exist %httpd_ssl_cnf% (
	echo [33mCreation of the conf file %httpd_ssl_cnf% [0m
	(
		echo Listen 443
		echo LoadModule ssl_module modules/mod_ssl.so
		echo SSLRandomSeed startup builtin
		echo SSLRandomSeed connect builtin
		echo SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL  
		echo:
	) > %httpd_ssl_cnf%
)

if not exist %openssl_ext_cnf% (
	echo [33mCreation of the conf file %openssl_ext_cnf% [0m
	echo basicConstraints = CA:FALSE >> %openssl_ext_cnf%
	echo keyUsage = nonRepudiation, digitalSignature, keyEncipherment >> %openssl_ext_cnf%
	echo subjectAltName = $ENV::ALTNAME >> %openssl_ext_cnf%
)

if not exist %ca_crt% (
	echo [33mCreation of the certificate authority %ca_crt% [0m
	"%opensslexe%" req -x509 -sha256 -new -out %ca_crt% -keyout %ca_key% -config %openssl_cnf% -days 3650 -subj "/C=FR/ST=MyRegion/L=MyLocality/O=MyCompany/CN=%cadomain%" -passin pass:%passphrase% -passout pass:%passphrase%
	certutil -addstore -f -enterprise -user root %ca_crt%
)

echo [33mCreation of the certificates [0m
"%opensslexe%" req -newkey rsa:2048 -out %domain_csr% -pubkey -new -keyout %domain_key% -sha256 -config %openssl_cnf% -subj "/C=FR/ST=MyRegion/L=MyLocality/O=MyCompany/CN=%domain%" -passin pass:%passphrase% -passout pass:%passphrase%
"%opensslexe%" x509 -req -in %domain_csr% -CA %ca_crt% -CAkey %ca_key% -CAcreateserial -out %domain_crt% -days 3650 -sha256 -extfile %openssl_ext_cnf% -passin pass:%passphrase%
"%opensslexe%" rsa -in %domain_key% -passin pass:%passphrase% -passout pass:%passphrase% > %domain_key_nopass% 

PAUSE

(
	echo ^<VirtualHost *:80^>
	echo     ServerName "%domain%"
	echo     DocumentRoot "%documentroot%"
	echo:
	echo     ^<Directory "%documentroot%/"^>
	echo         Options -Indexes +FollowSymLinks +MultiViews
    echo         AllowOverride All
    echo         Require all granted
    echo     ^</Directory^>
    echo:
    echo     ^# Possible values include: debug, info, notice, warn, error, crit,
    echo     ^# alert, emerg.
    echo     LogLevel warn
    echo:
    echo     ErrorLog "%logs%/error.log"
    echo     CustomLog "%logs%/access.log" common
    echo     php_value error_log "%logs%/error.log" 
	echo ^</VirtualHost^>
	echo:
	echo ^<VirtualHost *:443^>  
	echo     ServerName "%domain%"
	echo     DocumentRoot "%documentroot%"
	echo     SSLEngine on
	echo     SSLCertificateFile "%domain_crt%"
	echo     SSLCertificateKeyFile "%domain_key_nopass%"
	echo     SSLCACertificateFile "%ca_crt%"
    echo:
    echo     ErrorLog "%logs%/error.log"
    echo     CustomLog "%logs%/access.log" common
    echo     php_value error_log "%logs%/error.log" 
	echo ^</VirtualHost^>
	echo:
) >> %httpd_ssl_cnf%

echo [33mSelf-signed certificates created for the domain %domain% in the folder %destination%. [0m
echo:
echo ===================
echo:

PAUSE

echo [33mYou need to uncomment "extension=php_openssl.dll" in your "php.ini". [0m

PAUSE

echo [33mYou need to add "Include %httpd_ssl_cnf%" in your "httpd.conf": [0m

PAUSE

echo [33mYou need to restart your Apache server: [0m

PAUSE